import React, { Component } from 'react';
import ButtonModule from './modules/ButtonModule';
import ButtonToolbarModule from './modules/ButtonToolbarModule';
import Calculator from './modules/Calculator';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">

        <Calculator />
        
      </div>
    );
  }
}

export default App;
