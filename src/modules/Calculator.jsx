import React, { Component } from 'react';
import ButtonToolbarModule from './ButtonToolbarModule';
import {Grid,Row,Col} from 'react-bootstrap';

export default class Calculator extends Component {

    render() {
        let i = 0;
        return (
            <div style={{height:"280px", width:"280px"}}>
                <div>
                    <h2 className="calScreen">{i}</h2>
                </div>
                <div>
                    <ButtonToolbarModule btnVals={['AC', '+/-', '%', '/']} />
                    <ButtonToolbarModule btnVals={['7', '8', '9', '*']} />
                    <ButtonToolbarModule btnVals={['4', '5', '6', '-']} />
                    <ButtonToolbarModule btnVals={['1', '2', '3', '+']} />
                    <ButtonToolbarModule btnVals={['0', '.', '=']} />
                </div>
            </div>

            // <Grid>
            //     <Row className="show-grid">
            //         <Col xs={6} md={4}>
            //             <code>&lt;{'Col xs={6} md={4}'} /&gt;</code>
            //         </Col>
            //         <Col xs={6} md={4}>
            //             <code>&lt;{'Col xs={6} md={4}'} /&gt;</code>
            //         </Col>
            //         <Col xsHidden md={4}>
            //             <code>&lt;{'Col xsHidden md={4}'} /&gt;</code>
            //         </Col>
            //     </Row>
            // </Grid>


        );
    }
}