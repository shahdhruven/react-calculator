import React, {Component} from 'react';
import {Button, ButtonToolbar, Col} from 'react-bootstrap';

export default class ButtonModule extends Component{
    
    render(){
        return(
            <Col>
                <Button 
                    bsSize="large" 
                    class="btn-block" 
                    style={{height:"70px", width:"70px" }}
                >
                {this.props.showVal}
                </Button>
            </Col>
        );
    }
} 