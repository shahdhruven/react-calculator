import React, { Component } from 'react';
import ButtonModule from './ButtonModule';
import {Button, ButtonGroup, ButtonToolbar} from 'react-bootstrap';

export default class ButtonToolbarModule extends Component{
    
    render(){

        const btns = [];

        for(let i = 0; i < this.props.btnVals.length; ++i) {
                 
            btns.push(
                <ButtonModule
                    showVal={this.props.btnVals[i]}
                />
            );
            
        }

        return(
            <div>
                <ButtonToolbar btnVals={this.props.btnVals}>
                    {btns}
                </ButtonToolbar>
            </div>
        );
    }
}
